﻿using UnityEngine;
using System.Collections;

public class SimonSaysScript : MonoBehaviour {

	//public KeyCode[] key = {KeyCode.A, KeyCode.B, KeyCode.C ,KeyCode.D};
	public GameObject[] lights;
	GameObject[]  exceptions;
	float timeInterval = 1.0f;
	float timeBeforeStart = 1.0f;

	void Start () {
		foreach (GameObject light in lights) {
			light.SetActive(false);
		}
		exceptions = new GameObject[lights.Length];
		InvokeRepeating("runSimonSays", timeBeforeStart, timeInterval);
	}

	void Update () 
	{
		/*
		//Control de usuario de las luces 
		if (key.Length == lights.Length) {
			for (int i = 0; i < key.Length; i++) {
				if (Input.GetKeyDown (key [i])) {
					if (lights [i].activeSelf == true) {
						lights [i].SetActive (false);
					} else {
						lights [i].SetActive (true);
					}
				}
			}
		} else {
			Debug.LogError("SimonSaysScript:Update--> lenght");
		}*/
	}

	/* Ejecuta el funcionamiento de las luces de SimonSays*/
	void runSimonSays() {
		GameObject randomLight = turnOnRandomLight (lights);
		exceptions [0] = randomLight;
		turnOffLights(exceptions);
	}

	/* Enciende una luz al azar y devulve su GameObject. Si la luz ya esta encendida, busca una diferente no encendida.*/
	GameObject turnOnRandomLight(GameObject[] lights) {
		int maxRandom = lights.Length;
		int position = 0;
		bool foundNewLight = false;

		if (lights.Length > 2) {
			while (!foundNewLight) {
				position = Random.Range (0, maxRandom);
				if (lights [position].activeSelf == false) {
					foundNewLight = true;
				}
			}
			lights [position].SetActive (true);
		} else {
			lights [position].SetActive (true);
		}
		return lights [position];
	}

	/* Enciende todas las luces menos las recibidas como parametro*/
	void turnOnLights(GameObject[] exceptions) {
		bool blackListed;
		foreach(GameObject light in lights) {
			blackListed = false;;
			foreach(GameObject exception in exceptions){
				if (light == exception) {
					blackListed = true;
				}
			}	
			if (!blackListed){
				light.SetActive(true);
			}
		}
	}

	/* Apaga todas las luces menos las recibidas como parametro*/
	void turnOffLights(GameObject[] exceptions) {
		bool blackListed;
		foreach(GameObject light in lights) {
			blackListed = false;;
			foreach(GameObject exception in exceptions){
				if (light == exception) {
					blackListed = true;
				}
			}	
			if (!blackListed){
				light.SetActive(false);
			}
		}
	}
}