﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIScript : MonoBehaviour {

	public void goToTicTacToe() {
		Application.LoadLevel("SceneTicTacToe");
	}

	public void goToLunarBomb() {
		Application.LoadLevel("SceneLunarBomb");
	}

	public void goToBasicLayout() {
		Application.LoadLevel("SceneBasicLayout");
	}

	public void goToSimonSays() {
		Application.LoadLevel("SceneSimonSays");
	}
	
	public void goToMainMenu() {
		Application.LoadLevel("SceneMenu");
	}

	public void quitGame() {
		Application.Quit();
	}
}
